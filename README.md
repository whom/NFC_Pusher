# NFC Pusher
Distribute files via NFC!
Leave Wifi, Cellular data behind!
No reception? no problem!
 
##  Why NFC Pusher?
NFC is a low power, proximity based transmission.
Since one needs near physical contact to use this medium, it is ideal for high sensitivity data.
This app will enable you to distribute your secret/key/token/etc to your confidante without remote eavesdropping or the threat of man in the middle attacks.
 
## Permissions, Requirements
This application requires access to NFC, and permissions to both read and write to storage.
This application will not prompt you to set them, rather it requires you to set them before hand.
This application requires an API level of at least 19. (Android 4.4)
 
## Building
I have included all project files for Android Studio.
 
## Contributions Welcome!
Feel free to lodge an issue if you have any questions or comments.
Please let me know how I can help you contribute to this project! 