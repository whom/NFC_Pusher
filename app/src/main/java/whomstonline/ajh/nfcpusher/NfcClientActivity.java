package whomstonline.ajh.nfcpusher;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcF;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

public class NfcClientActivity extends Activity{

    // stuff for foreground dispatch of intent filtering
    private IntentFilter[] intentFiltersArray = null;
    private PendingIntent pendingIntent = null;
    private NfcAdapter mNfcAdapter = null;
    private String[][] techListsArray = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_nfc_client);


        // set adapter
        // catch an oopsie woopsie
        mNfcAdapter = NfcAdapter.getDefaultAdapter(getApplicationContext());
        if (mNfcAdapter == null) {
            Toast.makeText(getApplicationContext(), "Couldnt get NFC adapter", Toast.LENGTH_LONG);
            return;
        }

        // prep for foreground Dispatch
        pendingIntent = PendingIntent.getActivity(
                this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        // set up the only intent filter we care about
        IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndef.addDataType("*/*");    /* Handles all MIME based dispatches.
                                       You should specify only the ones that you need. */

        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("fail", e);
        }

        //populate array of intents we will foreground filter for
        intentFiltersArray = new IntentFilter[]{ndef,};
        techListsArray = new String[][] { new String[] { Ndef.class.getName(), NdefFormatable.class.getName()} };
    }


    public void onPause() {
        super.onPause();
        mNfcAdapter.disableForegroundDispatch(this);
    }

    public void onResume() {
        super.onResume();
        mNfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
    }

    public void onNewIntent(Intent intent) {
        TextView textView = (TextView) findViewById(R.id.clientTextField);
        NdefMessage m = (NdefMessage) intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)[0];

        textView.setText("[+] Found NDEF Message! \n");
        textView.append(" - number of records: " + m.getRecords().length + "\n\n");

        for(NdefRecord r : m.getRecords()){
            // output record
            textView.append("[+] Record: " + new String(r.getType()) + "\n");

            if(r.getPayload().length < 100) {
                textView.append(new String(r.getPayload()));
            } else {
                textView.append("Payload Ommited for Brevity.\n\n");
            }

            // save file
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), new String(r.getType()));
            try {
                FileOutputStream f = new FileOutputStream(file);
                f.write(r.getPayload());

            }  catch (Exception e) {
                textView.append("\n[-] File Write Exception!\n" + e.getMessage() + "\n\n");
            }
        }
    }
}
