package whomstonline.ajh.nfcpusher;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, getIntent().getAction(), 1);
    }

    public void goServer(View v){
        Intent myIntent = new Intent(MainActivity.this, NfcServerActivity.class);
        MainActivity.this.startActivity(myIntent);
        finish();
    }

    public void goClient(View v){
        Intent myIntent = new Intent(MainActivity.this, NfcClientActivity.class);
        MainActivity.this.startActivity(myIntent);
        finish();
    }

    protected void onResume(){
        super.onResume();
        Toast.makeText(this, getIntent().getAction(), 0);
    }
}
