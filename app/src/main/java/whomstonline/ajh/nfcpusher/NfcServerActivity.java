package whomstonline.ajh.nfcpusher;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.InputStream;

//public class NfcServerActivity extends Activity implements NfcAdapter.CreateNdefMessageCallback {
public class NfcServerActivity extends Activity {
    private int FILE_READ_REQ = 42;
    private Uri uri = null;
    private byte[] cached_file = null;
    private NdefMessage message = null;
    private NfcAdapter mNfcAdapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_nfc_server);

        // set adapter
        // catch an oopsie woopsie
        mNfcAdapter = NfcAdapter.getDefaultAdapter(getApplicationContext());
        if (mNfcAdapter == null) {
            Toast.makeText(getApplicationContext(), "Couldnt get NFC adapter", Toast.LENGTH_LONG);
            return;
        }
    }

    public void startServer(View v) {

        // TODO: background file server from NFCServer Object
        Toast t = Toast.makeText(this, "Starting Server...", Toast.LENGTH_LONG);
        t.show();

        // generate message with file content
        message = new NdefMessage(NdefRecord.createExternal("whomst.online", queryName(getContentResolver(), uri), cached_file));
        Log.d("NFCPUSHSRV", "Created message of len: " + message.getByteArrayLength());

        // set push message
        mNfcAdapter.setNdefPushMessage(message, this);
        //mNfcAdapter.setBeamPushUris(new Uri[] {uri}, this);
        Log.d("NFCPUSHSRV", "Server is up");

    }

    // bound to pick file button
    public void showFileChooser(View v) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");      //all files
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), FILE_READ_REQ);

        } catch (android.content.ActivityNotFoundException ex) {
            Toast t = Toast.makeText(this, "Please install a file manager", Toast.LENGTH_SHORT);
        }
    }

    @Override
    // Used to choose files
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {

        if (requestCode == 42 && resultCode == Activity.RESULT_OK) {
            if (resultData != null) {

                // set uri to chosen file
                uri = resultData.getData();

                // set button title
                Button b = (Button) findViewById(R.id.serverSelectFileButton);
                b.setText(queryName(getContentResolver(), uri));

                // attempt to cache file
                try {

                    InputStream inputStream = getContentResolver().openInputStream(uri);
                    cached_file = new byte[inputStream.available()];
                    inputStream.read(cached_file);

                } catch (Exception e) {
                    //// TODO: logcat the heck outta this
                }
            }
        }
    }

    private String queryName(ContentResolver resolver, Uri uri) {
        Cursor returnCursor =
                resolver.query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }

    //@Override
    //public createNdefMessage createNdefMessage(NfcEvent event) {
    //    return message;
    //}
}
